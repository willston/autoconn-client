####Blue Bloc Designs Frameworks
![AutoCONN Promo Main.png](https://bitbucket.org/repo/75zej5/images/2078896831-AutoCONN%20Promo%20Main.png)
___

Super simple, event driven, Global Zero Configuation Network by Blue Bloc Designs
##Quick Start
###Summary
Setup is a three step procedure:  

1. Install the library
2. Register your program
3. Use the library

For now, we will do registration manually; however, this process is easily automated. 
###Step One: Installation
To install, download the .zip archive or clone the repository.  It will create a file structure like this:  

| -- `index.js `  
| -- `/node_modules/` 

This is all you need to get started.  Inside of `node_modules` is where the actual library is located, in a folder called `autoconn-client`.  If you want to add AutoCONN connectivity to your existing project, simply copy this folder into your `node_modules` folder for that project.

###Step Two: Registration
To register you need to write a small program.  Here is what it should contain:


##How it works:

AutoCONN// Works by using a secure web sockets connection to Blue Bloc Designs server.  On connection it registeres and annoucounces its presence to the group.  

To send a message, all you are required to do is say `auto.conn.send` and then pass the UID (unique address) of the device or application you are looking to send the message to and the javascript object you want to send.

##Security
AutoCONN// relies on `wss://` or secure websockets with a valid certificate.  For an extra layer of security, a code called an 'aa' code may be used.  If an 'aa' code is used, AutoCONN will automatically encrypt all data and you must enter the same 'aa' code on the recieving device for the message to be decrypted.  

Additionally, larger data objects may be optionally compressed by the Google Snappy engine.  This makes the data transfer much smaller and also makes data almost indistiguisable as it is now:  

* Encrypted and salted with an 'aa' code 
* Compressed with Google's Snappy engine
* Encrypted using `wss://`

If you see something which could be a security issue, please contact will@blueblocdesigns.ca 
##Usage
I'm making this all free to use, so please make good use of this system, but don't overload it please.   If you are creating your own product and would like to use AutoCONN// as a communication system, we can provide you with the server software.  Please just use this main service for use in testing, eductation and personal home use.  

Excessive use by the same UID's or by a manufacture will automatically be removed.
##Functions
###Starting the connection:
To start the connection, use the:  

`auto.conn.start(config);`

The config is javascript object and takes the following paramaters:  

* `id`  - this is the UID for your device
* `group` - this is the group your device belongs to.  It can only ask for data about devices in its group.
* `location` - this is an array with the current latititude and longitude of the device.
* `aa` - A unique code which only you know and the clients know to encrypt the data.

Here is an example of initiating communication:
```javascript
auto.conn.start({  
	id: "unregistered",  
	group: "0",  
	location: ["latitude", "longitude"],  
	aa: "somecode"  
});
```
