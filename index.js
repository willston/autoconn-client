var auto = require("autoconn-client");

//
// This initializes the connection with the server.  If the aa key option is not provided the system will send in plain text over SSL.
//

auto.conn.start({
        id: "REGISTRAR", //This is your devices unique identification number
        group: "13", //This is your group number.  You can only see devices in your group using the QUERY, but you can talk to anyone.
        location: ["latitude", "longitude"], //This is your devices location on the planet.  Please be real with this.
        aa: "TESTING" //This is a key to encrypt and decrypt the data.  Only you know it! 
});

//
// Anything in .begins happens when the server has made a connection.
//

auto.conn.begins = function(details) {
    //This will print the response from the server so 
    //you can see all the data given to you.
    console.log(JSON.stringify(details, null, 4)); 


    //
    // .sms sends an SMS message.  You simply state the carrier the same 
    // way as it is displayed in the title bar of your smart phone.
    //

    auto.conn.sms({
        carrier: "----", //Carrier as displayed on your phone.  Should be all caps.
        number: "----------", //The phone number.  No splace.  No symbols. Just numbers.
        message: "Testing AutoCONN EALS by Blue Bloc Designs" //Contents of the message. Less than 200 characters!
    });

    //
    // .send sends an SMS message.  You simply state the carrier the same 
    // way as it is displayed in the title bar of your smart phone.
    //

    auto.conn.send("697c5b79-8103", {
        some: "sort of data can go here",
        it: "can be any sort of JSON or just a string even!",
        but: "its going to get encrypted if you use the aa key!"
    }, true, /*  */
     function() {
        //This function will run when a signature from the recieving device has been sent.
        //delivered can be "TRUE" or "FALSE", and details contains how long it took to 
        //send the message as well as any responses from the device.
    });


}
   
//
// Anything in .rx happens when the server recieves a data packet.
// The data packet includes some data added headers.
//

auto.conn.rx = function(msg) {
    //This will print the response from the server so 
    //you can see all the data given to you.
    console.log(JSON.stringify(msg, null, 4)); 
}

auto.conn.err = function(err) {
    //This will print the response from the server so 
    //you can see all the data given to you.
    console.log(JSON.stringify(err, null, 4)); 
}